/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2012  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "tagmodel.h"

#include <Nepomuk2/Resource>
#include <Nepomuk2/ResourceManager>
#include <Nepomuk2/Variant>
#include <Nepomuk2/Query/Query>
#include <Nepomuk2/Query/QueryServiceClient>
#include <Nepomuk2/Query/ResourceTypeTerm>
#include <Nepomuk2/Query/ComparisonTerm>

#include <Soprano/Vocabulary/NAO>

using namespace Soprano::Vocabulary;

TagModel::TagModel(QObject* parent): QAbstractListModel(parent)
{
    repopulate();
}

void TagModel::repopulate()
{
    beginRemoveRows( QModelIndex(), 0, m_tags.size() );
    m_tags.clear();
    m_usageCount.clear();
    endRemoveRows();

    Nepomuk2::Query::ResourceTypeTerm typeTerm( NAO::Tag() );
    Nepomuk2::Query::ComparisonTerm labelTerm( NAO::identifier(), Nepomuk2::Query::Term() );
    labelTerm.setSortWeight( 1 );

    Nepomuk2::Query::Query query( typeTerm && labelTerm );
    Nepomuk2::Query::QueryServiceClient *client = new Nepomuk2::Query::QueryServiceClient(this);
    connect(client, SIGNAL(newEntries(QList<Nepomuk2::Query::Result>)), this, SLOT(newEntries(QList<Nepomuk2::Query::Result>)) );
    connect(client, SIGNAL(finishedListing()), client, SLOT(deleteLater()) );

    client->query( query );
}

void TagModel::newEntries(const QList< Nepomuk2::Query::Result >& results)
{
    beginInsertRows( QModelIndex(), m_tags.size(), m_tags.size() + results.size() - 1 );
    foreach( const Nepomuk2::Query::Result& result, results ) {
        m_tags << result.resource();
    }
    endInsertRows();
}

int TagModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED( parent );
    return m_tags.size();
}

QVariant TagModel::data(const QModelIndex& index, int role) const
{
    if( !index.isValid() || index.row() >= m_tags.size() )
        return QVariant();

    if( role == Qt::DisplayRole )
        return m_tags[ index.row() ].genericLabel();
    else if( role == NepomukUriRole )
        return m_tags[ index.row() ].uri();

    return QVariant();
}

void TagModel::rename(const QModelIndex& index, const QString& newLabel)
{
    if( !index.isValid() || index.row() >= m_tags.size() )
        return;

    Nepomuk2::Resource res = m_tags[ index.row() ];
    Nepomuk2::Variant value( newLabel );
    res.setProperty( NAO::identifier(), value );
    res.setProperty( NAO::prefLabel(), value );

    emit dataChanged( index, index );
}
