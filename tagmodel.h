/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2012  Vishesh Handa <handa.vish@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef TAGMODEL_H
#define TAGMODEL_H

#include <QtCore/QAbstractListModel>

#include <Nepomuk2/Tag>
#include <Nepomuk2/Query/Result>

class TagModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Role {
        NepomukUriRole = 23423
    };

    TagModel(QObject* parent = 0);
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    void repopulate();
    void rename(const QModelIndex& index, const QString& newLabel);

public slots:
    void newEntries(const QList<Nepomuk2::Query::Result>& results);

private:
    QList<Nepomuk2::Tag> m_tags;
    QList<int> m_usageCount;
};

#endif // TAGMODEL_H
